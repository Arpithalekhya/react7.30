import React from "react";
import "./App.css";
import ChangedPost from "./Components/ChangedPost";
import Footer from "./Components/Footer";
import Header from "./Components/Header";
import Menu from "./Components/Menu";

export default function App() {
  return (
    <div>
      <Header />
      <Menu />
      <Footer />
    </div>
  );
}
