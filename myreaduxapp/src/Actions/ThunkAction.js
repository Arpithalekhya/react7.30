import ServerCall from "../Service/ServerCallService";

const thunkAction = () => {
  return async (dispatch, state) => {
    console.log(dispatch, "dispatch");
    console.log(state, "state");
    const resp = await ServerCall.get(
      "https://jsonplaceholder.typicode.com/posts/1/comments"
    );
    dispatch({
      type: "COMMENTS",
      payload: resp.data
    });
  };
};

export default thunkAction;
