import ServerCall from "../Service/ServerCallService";
import store from "../Store/store";

export const postsAction = async () => {
  //   let res = await ServerCall.get("https://jsonplaceholder.typicode.com/posts");
  //   store.dispatch({
  //     type: "POSTS",
  //     payload: res.data
  //   });
  ServerCall.get("https://jsonplaceholder.typicode.com/posts")
    .then((res) => {
      store.dispatch({
        type: "POSTS",
        payload: res.data
      });
    })
    .catch(() => {
      store.dispatch({
        type: "POSTS",
        payload: []
      });
    });
};
