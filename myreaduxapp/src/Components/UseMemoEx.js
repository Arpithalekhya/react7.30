import React, {useMemo, useState} from "react";

export default function UseMemoEx() {
  const [count, setCount] = useState(0);
  const delay = useMemo(() => {
    for (let i = 0; i <= 1000000000; i++) {}
    return 500000;
  }, []);
  const handleInc = () => {
    setCount(count + 1);
  };
  return (
    <div>
      <button onClick={handleInc}>Inc Count</button>
      <h1>{count}</h1>
      <h1>{delay}</h1>
    </div>
  );
}
