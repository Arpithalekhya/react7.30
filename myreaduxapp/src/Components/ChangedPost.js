import React, {Component} from "react";
import store from "../Store/store";

export default class ChangedPost extends Component {
  changePost = () => {
    store.dispatch({
      type: "POSTS",
      payload: []
    });
  };
  render() {
    return (
      <div>
        <h1>
          <button onClick={this.changePost}>ChangePost</button>
        </h1>
      </div>
    );
  }
}
