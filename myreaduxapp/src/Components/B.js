import React, {Component} from "react";
import store from "../Store/store";
import {connect} from "react-redux";

class B extends Component {
  handleLocation = () => {
    let location = this.refs.loc.value;
    // store.dispatch({
    //   type: "LOC",
    //   payload: location
    // });
    this.props.disp({
      type: "LOC",
      data: location
    });
  };
  render() {
    return (
      <>
        <h1>I'm B Component</h1>
        <input ref="loc" />
        <button onClick={this.handleLocation}>SetLocation</button>
      </>
    );
  }
}
const mdp = (d) => {
  return {
    disp: d
  };
};
export default connect(null, mdp)(B);
