import React, {Component} from "react";
import thunkAction from "../Actions/ThunkAction";
import ServerCall from "../Service/ServerCallService";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

class ThunkExample extends Component {
  handleThunk = async () => {
    this.props.thunk();
  };
  render() {
    return (
      <div>
        <button onClick={this.handleThunk}>Click me to get comments</button>
      </div>
    );
  }
}
const mdp = (dispatch) => {
  return {
    thunk: bindActionCreators(thunkAction, dispatch)
  };
};

export default connect(null, mdp)(ThunkExample);
