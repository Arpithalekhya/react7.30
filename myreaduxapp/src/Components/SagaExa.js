import React, {Component} from "react";
import store from "../Store/store";

class SagaExample extends Component {
  handleSaga = () => {
    debugger;
    store.dispatch({
      type: "COMMENT"
    });
  };
  render() {
    return (
      <div>
        <button onClick={this.handleSaga}> get comments</button>
      </div>
    );
  }
}

export default SagaExample;
