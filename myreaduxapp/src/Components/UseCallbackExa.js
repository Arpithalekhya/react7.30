import React, {useState, memo, useCallback} from "react";

export default function UseCallbackExa() {
  const [count, setCount] = useState(0);
  const handleInc = () => {
    setCount(count + 1);
  };
  const sampleMethod = useCallback(() => {
    console.log("Sachin");
  }, []);
  return (
    <div>
      {alert("Parent called")}
      UseCallbackExa
      <button onClick={handleInc}>Inc Count</button>
      <h1>{count}</h1>
      <Child sampleMethod={sampleMethod} />
    </div>
  );
}

const Child = memo(() => {
  return (
    <>
      {alert("Child called")}
      I'm Child Component
    </>
  );
});
