import React, {Component} from "react";
import {connect} from "react-redux";
import A from "./A";
import B from "./B";
import C from "./C";

class ReduxfirstExa extends Component {
  state = {
    data: []
  };
  getPosts = () => {
    this.setState({
      data: this.props.myPosts
    });
  };
  render() {
    return (
      <div>
        <A />
        <B />
        <C />
        <button onClick={this.getPosts}>GetDataPosts</button>
        {this.state.data.map((obj, i) => {
          return (
            <h1 key={i}>
              {obj.id}...{obj.title}
            </h1>
          );
        })}
      </div>
    );
  }
}
const msp = (store) => {
  return {
    myPosts: store.r.posts
  };
};

export default connect(msp)(ReduxfirstExa);
