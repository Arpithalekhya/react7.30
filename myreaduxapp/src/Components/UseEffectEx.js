import React, {useEffect, useState} from "react";

export default function UseEffectEx({loc}) {
  const [count, incCount] = useState(0);
  const [name, setName] = useState("Sachin");
  const [data, setData] = useState([]);
  const [isChild, setIsChild] = useState(true);
  useEffect(() => {
    alert("componentDidMount");
    fetch("https://jsonplaceholder.typicode.com/posts")
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        setData(data);
      });
  }, []); //componentDidMount

  useEffect(() => {
    alert("componentDidUpdate for name");
  }, [name, loc]); //componentDidUpdate

  useEffect(() => {
    alert("componentDidUpdate for count");
  }, [count]); //componentDidUpdate

  const handleInc = () => {
    incCount(count + 1);
  };
  const handleNameChange = () => {
    setName("Kohli");
  };

  const handleChild = () => {
    setIsChild(!isChild);
  };
  return (
    <div>
      <p>
        {isChild && <Child />}
        <button onClick={handleChild}>I'm going to mount/unmount</button>
      </p>
      UseEffect
      <button onClick={handleInc}>Inc Count</button>
      <button onClick={handleNameChange}>Name Change</button>
      <h1>{count}</h1>
      <h1>{name}</h1>
      <ol>
        {data?.map((dataObj, i) => {
          return <li key={i}>{dataObj.title}</li>;
        })}
      </ol>
    </div>
  );
}

const Child = () => {
  useEffect(() => {
    return () => {
      alert("I'm unmounting");
    };
  }, []); //componentWillUnmount
  return <h1 style={{color: "blueviolet"}}>I'm a Child Component</h1>;
};
