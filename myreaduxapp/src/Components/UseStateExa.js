import React, {useState} from "react";

export default function UseStateExa() {
  const [count, setCount] = useState(0);
  const [name, setName] = useState("");
  const handleInc = () => {
    setCount(count + 1);
    setName("DHONI");
  };
  return (
    <>
      <button onClick={handleInc}>Inc Count</button>
      <h1>
        {count}......{name}
      </h1>
      <MyExampleUseState />
    </>
  );
}

const MyExampleUseState = () => {
  const [state, setState] = useState({
    salary: 0,
    age: 0
  });
  const handleAge = () => {
    // setState((pre) => ({...pre, age: pre.age + 100}));
    setState((prev) => {
      return {
        ...prev,
        age: prev.age + 100
      };
    });
  };
  const handleSalary = () => {
    setState((pre) => ({...pre, salary: pre.salary + 1000}));
  };
  return (
    <>
      <button onClick={handleAge}>Inc Age</button>
      <button onClick={handleSalary}>Inc Salary </button>
      <h1>{state.salary}....salary</h1>
      <h1>{state.age}...age</h1>
    </>
  );
};
