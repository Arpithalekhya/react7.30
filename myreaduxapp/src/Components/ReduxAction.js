import React, {Component} from "react";
import {postsAction} from "../Actions/PostAction";
import ServerCall from "../Service/ServerCallService";
import store from "../Store/store";

export default class ReduxAction extends Component {
  constructor() {
    super();
    this.state = {
      data: []
    };
  }
  handleAjax = async () => {
    // axios
    //   .get("https://jsonplaceholder.typicode.com/posts")
    //   .then((res) => {
    //     console.log(res.data, "response");
    //   })
    //   .catch((res) => {
    //     console.log(res);
    //   });

    // let res = await axios.get("https://jsonplaceholder.typicode.com/posts");
    // console.log(res.data, "response");
    // let res = await ServerCall.get(
    //   "https://jsonplaceholder.typicode.com/posts"
    // );
    // console.log(res, "response");
    // store.dispatch({
    //   type: "POSTS",
    //   payload: res.data
    // });
    postsAction();
  };

  getPosts = () => {
    const state = store.getState();
    const post = state.r.posts;
    this.setState({
      data: post
    });
  };
  render() {
    return (
      <div>
        <button onClick={this.handleAjax}>HandleAjax</button>
        <button onClick={this.getPosts}>GetPost </button>
        {this.state.data.map((obj) => {
          return <h1>{obj.title}</h1>;
        })}
      </div>
    );
  }
}
