import React from "react";
import {Link, HashRouter, Route, Routes} from "react-router-dom";
import ReduxFirstExample from "../Components/ReduxfirstExa";
import UseStateExample from "../Components/UseStateExa";
import UseEffectEx from "./UseEffectEx";
import UseRefEx from "./UseRefEx";
import UseCallbackExa from "./UseCallbackExa";
import UseMemoEx from "./UseMemoEx";
import ReduxAction from "./ReduxAction";
import ThunkExample from "./ThunkExample";
import SagaExample from "./SagaExa";

export default function Menu() {
  return (
    <div>
      <HashRouter>
        <div className="menu-items">
          <Link to="/redux_app">Redux Sample App</Link>
          <Link to="/useState">UseState</Link>
          <Link to="/useEffect">UseEffect</Link>
          <Link to="/useRef">UseRef</Link>
          <Link to="/useCB">UseCallBack</Link>
          <Link to="/useMemo">UseMemo</Link>
          <Link to="/reduxAction">ReduxAction</Link>
          <Link to="/reduxThunk">ReduxThunk</Link>
          <Link to="/reduxSaga">ReduxSaga</Link>
        </div>
        <Routes>
          <Route path="/redux_app" element={<ReduxFirstExample />} />
          <Route path="/useState" element={<UseStateExample />} />
          <Route path="/useEffect" element={<UseEffectEx />} />
          <Route path="/useRef" element={<UseRefEx />} />
          <Route path="/useCB" element={<UseCallbackExa />} />
          <Route path="/useMemo" element={<UseMemoEx />} />
          <Route path="/reduxAction" element={<ReduxAction />} />
          <Route path="/reduxThunk" element={<ThunkExample />} />
          <Route path="/reduxSaga" element={<SagaExample />} />
        </Routes>
      </HashRouter>
    </div>
  );
}
