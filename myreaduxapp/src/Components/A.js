import React, {Component} from "react";
import store from "../Store/store";
import {connect} from "react-redux";

class A extends Component {
  handleName = () => {
    debugger;
    let name = this.refs.name.value;
    // store.dispatch({
    //   type: "NAME",
    //   payload: name
    // });
    this.props.dis({
      type: "NAME",
      payload: name
    });
  };
  render() {
    return (
      <div>
        <h1>I'm A Component</h1>
        <input ref="name" />
        <button onClick={this.handleName}>SetName</button>
      </div>
    );
  }
}
// const msp = (state) => {
//  return {
//    n:state.r.name,
//    l:state.r.location
//  }
// };
const mdp = (dispatch) => {
  return {
    dis: dispatch
  };
};
export default connect(null, mdp)(A);
