import React, {useRef} from "react";

export default function UseRefEx() {
  const myRef = useRef(null);
  const myRef1 = React.createRef(null);
  const fnClick = () => {
    alert(myRef.current.value);
    alert(myRef1.current.innerText);
  };
  return (
    <div>
      <input ref={myRef} />
      <h1 ref={myRef1}>Sachin</h1>
      <button onClick={fnClick}>Click me</button>
    </div>
  );
}
