import {takeLatest, call, put} from "redux-saga/effects";
import ServerCall from "../../Service/ServerCallService";
function* commentSaga() {
  debugger;
  const res = yield call(
    ServerCall.get,
    "https://jsonplaceholder.typicode.com/posts/1/comments"
  );
  yield put({
    type: "MYCOMMENTS",
    payload: res.data
  });
}

function* postSaga() {}

function* myFistSaga() {
  yield takeLatest("COMMENT", commentSaga);
  yield takeLatest("POST", postSaga);
}
export default myFistSaga;
