import {all} from "redux-saga/effects";
import myFistSaga from "./CommentSaga";

function* rootSaga() {
  yield all([myFistSaga()]);
}

export default rootSaga;
