import axios from "axios";

class ServerCall {
  static get(url) {
    debugger;
    return axios.get(url);
  }
  static post(url, data) {
    return axios.post(url, data);
  }
}

// const obj = new ServerCall();

// export default obj;

export default ServerCall;
