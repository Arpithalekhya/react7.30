import {combineReducers} from "redux";
import reducer from "./reducer";
import paymentReducer from "./paymentReducer";

const rootReducer = combineReducers({
  r: reducer,
  payment: paymentReducer
});

export default rootReducer;
